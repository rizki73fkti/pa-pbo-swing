package soal;

import java.util.ArrayList;

public class JawabanPendaftar {
    private ArrayList<String> jawaban = new ArrayList<>();
    private float nilai = 0;
    

    public void setJawaban(ArrayList<String> jawaban) {
        this.jawaban = jawaban;
    }
    
    public ArrayList<String> getJawaban() {
        return jawaban;
    }
    
    public String getJawabanIndex(int i) {
        return jawaban.get(i);
    }

    public void setNilai(float nilai) {
        this.nilai = nilai;
    }
    
    public float getNilai() {
        return nilai;
    }
    
    
}
