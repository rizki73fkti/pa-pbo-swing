package soal;

public class Soal {
    private String question, jawaban_A, jawaban_B, jawaban_C, jawaban_D, jawaban_E, jawabanBenar;
    private float nilai;
    
    public Soal(
            String question, 
            String jawaban_A, 
            String jawaban_B, 
            String jawaban_C, 
            String jawaban_D, 
            String jawaban_E, 
            String jawabanBenar ) {
        
        this.question = question;
        this.jawaban_A = jawaban_A;
        this.jawaban_B = jawaban_B;
        this.jawaban_C = jawaban_C;
        this.jawaban_D = jawaban_D;
        this.jawaban_E = jawaban_E;
        this.jawabanBenar = jawabanBenar;
        
        
    }
    
    //SETTER
    public void setQuestion(String question) {
        this.question = question;
    }

    public void setJawaban_A(String jawaban_A) {
        this.jawaban_A = jawaban_A;
    }

    public void setJawaban_B(String jawaban_B) {
        this.jawaban_B = jawaban_B;
    }

    public void setJawaban_C(String jawaban_C) {
        this.jawaban_C = jawaban_C;
    }

    public void setJawaban_D(String jawaban_D) {
        this.jawaban_D = jawaban_D;
    }

    public void setJawaban_E(String jawaban_E) {
        this.jawaban_E = jawaban_E;
    }
    
    public void setJawabanBenar (String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
    }
    
    public void setNilai(float nilai) {
        this.nilai = nilai;
    }
    
    
    //GETTER
    public String getQuestion() {
        return question;
    }

    public String getJawaban_A() {
        return jawaban_A;
    }

    public String getJawaban_B() {
        return jawaban_B;
    }

    public String getJawaban_C() {
        return jawaban_C;
    }

    public String getJawaban_D() {
        return jawaban_D;
    }

    public String getJawaban_E() {
        return jawaban_E;
    }

    public String getJawabanBenar() {
        return jawabanBenar;
    }

    public float getNilai() {
        return nilai;
    }
    
}
