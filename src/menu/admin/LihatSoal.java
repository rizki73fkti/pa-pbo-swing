/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.admin;
import config.ModeChanger;
import config.Settings;

/**
 *
 * @author Ri_zki
 */
public class LihatSoal extends javax.swing.JFrame implements ModeChanger{

    private int index = 0;
    private String jawabanBenar;
    
    private String getJawabanBenar() {
        return jawabanBenar;
    }
    private void setJawabanBenar(String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
        answerA_RadioBtn.setSelected(jawabanBenar.equals("a"));
        answerB_RadioBtn.setSelected(jawabanBenar.equals("b"));
        answerC_RadioBtn.setSelected(jawabanBenar.equals("c"));
        answerD_RadioBtn.setSelected(jawabanBenar.equals("d"));
        answerE_RadioBtn.setSelected(jawabanBenar.equals("e"));
    }
    
    String[] questionStrings = new String[ data.DataSoal.getArraySoal().size() ];
        
    private void setQuestionStrings() {
        for(int i=0; i < data.DataSoal.getArraySoal().size(); i++){
            this.questionStrings[i] = Integer.toString(i+1);
        }
    }
    
    javax.swing.AbstractListModel<String> questionListModel = new javax.swing.AbstractListModel<String>() {
        String[] strings = questionStrings;
        
        @Override
        public int getSize() { return strings.length; }

        @Override
        public String getElementAt(int i) { return strings[i]; }
    };
    
    
    /**
     * 
     */
    public LihatSoal() {
        super("Aplikasi Pendaftaran CPNS 2020");
        
        this.setQuestionStrings();
        
        initComponents();
        changeMode();

        questionTextArea.setEditable(false);
        
        displaySoal();
        toggleQuestionNavigator();
        
        if(config.TestCpnsConfig.isBukaTes()){
            ubahSoalButton.setEnabled(false);
            hapusSoalButton.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        background = new javax.swing.JPanel();
        navBar = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        toggleModeBtn = new javax.swing.JButton();
        questionPanel = new javax.swing.JPanel();
        questionLabel = new javax.swing.JLabel();
        ubahSoalButton = new javax.swing.JButton();
        hapusSoalButton = new javax.swing.JButton();
        questionTextArea = new javax.swing.JTextArea();
        jawabanLabel = new javax.swing.JLabel();
        answerA_RadioBtn = new javax.swing.JRadioButton();
        answerB_RadioBtn = new javax.swing.JRadioButton();
        answerC_RadioBtn = new javax.swing.JRadioButton();
        answerD_RadioBtn = new javax.swing.JRadioButton();
        answerE_RadioBtn = new javax.swing.JRadioButton();
        prevSoalButton = new javax.swing.JButton();
        returnToMenuButton = new javax.swing.JButton();
        nextSoalButton = new javax.swing.JButton();
        questionListScrollPane = new javax.swing.JScrollPane();
        questionList = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(500, 350));

        background.setBackground(new java.awt.Color(230, 230, 240));
        background.setPreferredSize(new java.awt.Dimension(650, 600));

        navBar.setBackground(new java.awt.Color(0, 102, 153));
        navBar.setToolTipText("");

        title.setFont(new java.awt.Font("Avenir", 0, 20)); // NOI18N
        title.setForeground(new java.awt.Color(240, 240, 250));
        title.setText("Aplikasi Pendaftaran CPNS 2020");

        toggleModeBtn.setText("Dark Mode");
        toggleModeBtn.setEnabled(false);
        toggleModeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toggleModeBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout navBarLayout = new javax.swing.GroupLayout(navBar);
        navBar.setLayout(navBarLayout);
        navBarLayout.setHorizontalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(toggleModeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        navBarLayout.setVerticalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toggleModeBtn)
                    .addComponent(title))
                .addGap(533, 533, 533))
        );

        questionPanel.setBackground(new java.awt.Color(0, 153, 204));
        questionPanel.setEnabled(false);
        questionPanel.setPreferredSize(new java.awt.Dimension(200, 150));

        questionLabel.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        questionLabel.setForeground(new java.awt.Color(240, 240, 240));
        questionLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        questionLabel.setText("Pertanyaan 1 dari 50");

        ubahSoalButton.setBackground(hapusSoalButton.getBackground());
        ubahSoalButton.setText("Ubah");
        ubahSoalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ubahSoalButtonActionPerformed(evt);
            }
        });

        hapusSoalButton.setText("Hapus");
        hapusSoalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusSoalButtonActionPerformed(evt);
            }
        });

        questionTextArea.setBackground(new java.awt.Color(0, 153, 204));
        questionTextArea.setColumns(20);
        questionTextArea.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        questionTextArea.setForeground(new java.awt.Color(230, 230, 240));
        questionTextArea.setLineWrap(true);
        questionTextArea.setRows(5);
        questionTextArea.setText("Object adalah instance dari class.\nDari class Fruit object apakah yang mungkin dibuat?");
        questionTextArea.setBorder(null);

        jawabanLabel.setFont(new java.awt.Font("Avenir", 0, 16)); // NOI18N
        jawabanLabel.setForeground(new java.awt.Color(240, 240, 240));
        jawabanLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jawabanLabel.setText("Jawaban");

        answerA_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerA_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerA_RadioBtn.setSelected(true);
        answerA_RadioBtn.setText("a. Jawaban 1");
        answerA_RadioBtn.setEnabled(false);

        answerB_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerB_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerB_RadioBtn.setText("b. Jawaban 2");
        answerB_RadioBtn.setEnabled(false);

        answerC_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerC_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerC_RadioBtn.setText("c. Jawaban 3");
        answerC_RadioBtn.setEnabled(false);

        answerD_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerD_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerD_RadioBtn.setText("d. Jawaban 4");
        answerD_RadioBtn.setEnabled(false);

        answerE_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerE_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerE_RadioBtn.setText("e. Jawaban 5");
        answerE_RadioBtn.setEnabled(false);

        prevSoalButton.setBackground(new java.awt.Color(255, 153, 0));
        prevSoalButton.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        prevSoalButton.setText("˂");
        prevSoalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevSoalButtonActionPerformed(evt);
            }
        });

        returnToMenuButton.setText("Kembali");
        returnToMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnToMenuButtonActionPerformed(evt);
            }
        });

        nextSoalButton.setBackground(new java.awt.Color(255, 153, 0));
        nextSoalButton.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        nextSoalButton.setText("˃");
        nextSoalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextSoalButtonActionPerformed(evt);
            }
        });

        questionList.setBackground(new java.awt.Color(0, 153, 204));
        questionList.setModel(questionListModel);
        questionList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                questionListMouseClicked(evt);
            }
        });
        questionListScrollPane.setViewportView(questionList);

        javax.swing.GroupLayout questionPanelLayout = new javax.swing.GroupLayout(questionPanel);
        questionPanel.setLayout(questionPanelLayout);
        questionPanelLayout.setHorizontalGroup(
            questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionPanelLayout.createSequentialGroup()
                .addComponent(questionListScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jawabanLabel)
                            .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(questionLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(questionPanelLayout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(answerE_RadioBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(answerB_RadioBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                                        .addComponent(answerA_RadioBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(answerC_RadioBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(answerD_RadioBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(questionPanelLayout.createSequentialGroup()
                                                .addComponent(ubahSoalButton, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(hapusSoalButton, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(prevSoalButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(nextSoalButton))
                                            .addComponent(questionTextArea, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addGap(203, 203, 203)
                        .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        questionPanelLayout.setVerticalGroup(
            questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionPanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(questionLabel)
                    .addComponent(ubahSoalButton)
                    .addComponent(hapusSoalButton)
                    .addComponent(nextSoalButton)
                    .addComponent(prevSoalButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addComponent(questionTextArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(jawabanLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(answerA_RadioBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(answerB_RadioBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(answerC_RadioBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(answerD_RadioBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(answerE_RadioBtn)
                        .addGap(40, 40, 40)
                        .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52))
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addComponent(questionListScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout backgroundLayout = new javax.swing.GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(navBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(questionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        backgroundLayout.setVerticalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addComponent(navBar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(questionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void changeMode() {                                            
        if ( Settings.isDarkModeSelected() ) { 
            toggleModeBtn.setText("Light Mode");
            navBar.setBackground(new java.awt.Color(0,51,102));
            questionPanel.setBackground(new java.awt.Color(0,102,153));
            background.setBackground(new java.awt.Color(30, 30, 30));
            
            questionList.setBackground(new java.awt.Color(0,102,153));
            questionTextArea.setBackground(new java.awt.Color(0,102,153));
            
        } else { 
            toggleModeBtn.setText("Dark Mode");
            navBar.setBackground(new java.awt.Color(0,102,153));
            questionPanel.setBackground(new java.awt.Color(0,153, 204));
            background.setBackground(new java.awt.Color(230, 230, 240));
            
            questionList.setBackground(new java.awt.Color(0, 153, 204));
            questionTextArea.setBackground(new java.awt.Color(0, 153, 204));
        }
    }
    
    private void toggleQuestionNavigator(){
        prevSoalButton.setEnabled( !(index == 0));
        nextSoalButton.setEnabled( !(index == data.DataSoal.getArraySoal().size() - 1));
    }
    
    private void displaySoal(){
        questionList.setSelectedIndex(index);
        
        questionLabel.setText("Soal " + (index+1)+ " dari " + data.DataSoal.getArraySoal().size());
       
        /*
        scoreLabel.setText("Nilai per soal: " + 
                String.format("%.2f" ,data.DataSoal.getArrayIndex(index).getNilai())
        );
        */
        
        questionTextArea.setText(data.DataSoal.getArrayIndex(index).getQuestion());
        answerA_RadioBtn.setText("a. " + data.DataSoal.getArrayIndex(index).getJawaban_A());
        answerB_RadioBtn.setText("b. " + data.DataSoal.getArrayIndex(index).getJawaban_B());
        answerC_RadioBtn.setText("c. " + data.DataSoal.getArrayIndex(index).getJawaban_C());
        answerD_RadioBtn.setText("d. " + data.DataSoal.getArrayIndex(index).getJawaban_D());
        answerE_RadioBtn.setText("e. " + data.DataSoal.getArrayIndex(index).getJawaban_E());
        
        this.setJawabanBenar( data.DataSoal.getArrayIndex(index).getJawabanBenar() );
        
        
    }
    
    private void toggleModeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleModeBtnActionPerformed
        Settings.changeToggleMode();
        changeMode();
    }//GEN-LAST:event_toggleModeBtnActionPerformed

    private void returnToMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnToMenuButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_returnToMenuButtonActionPerformed

    private void ubahSoalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ubahSoalButtonActionPerformed
        CreateUpdateSoal menuTambahSoal = new CreateUpdateSoal( "ubah", index );
        menuTambahSoal.setVisible(true);
        menuTambahSoal.setLocationRelativeTo(null);
        this.dispose();
    }//GEN-LAST:event_ubahSoalButtonActionPerformed

    
    private void hapusSoalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusSoalButtonActionPerformed
        if(
            javax.swing.JOptionPane.showConfirmDialog(
                    null, 
                    "Anda yakin ingin menghapus soal nomor " + (index+1) +  " ?",
                    "Hapus Soal Tes", 
                    javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION
        ){
            data.DataSoal.hapusData(index);
            javax.swing.JOptionPane.showMessageDialog(null, 
                    "Soal nomor " + (index+1) +  " berhasil dihapus",
                    "Hapus Soal Tes", 
                    javax.swing.JOptionPane.INFORMATION_MESSAGE); 
            this.dispose();
        }
    }//GEN-LAST:event_hapusSoalButtonActionPerformed

    private void prevSoalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevSoalButtonActionPerformed
        if (index != 0 ){
            index--;

            displaySoal();
            toggleQuestionNavigator();
        }
    }//GEN-LAST:event_prevSoalButtonActionPerformed

    private void nextSoalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextSoalButtonActionPerformed
        if (index != data.DataSoal.getArraySoal().size() - 1 ){
            index++;

            displaySoal();
            toggleQuestionNavigator();
        }
    }//GEN-LAST:event_nextSoalButtonActionPerformed

    private void questionListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_questionListMouseClicked
        index = Integer.parseInt( questionList.getSelectedValue() ) - 1;
        displaySoal();
        toggleQuestionNavigator();
    }//GEN-LAST:event_questionListMouseClicked


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LihatSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LihatSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LihatSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LihatSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LihatSoal().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton answerA_RadioBtn;
    private javax.swing.JRadioButton answerB_RadioBtn;
    private javax.swing.JRadioButton answerC_RadioBtn;
    private javax.swing.JRadioButton answerD_RadioBtn;
    private javax.swing.JRadioButton answerE_RadioBtn;
    private javax.swing.JPanel background;
    private javax.swing.JButton hapusSoalButton;
    private javax.swing.JLabel jawabanLabel;
    private javax.swing.JPanel navBar;
    private javax.swing.JButton nextSoalButton;
    private javax.swing.JButton prevSoalButton;
    private javax.swing.JLabel questionLabel;
    private javax.swing.JList<String> questionList;
    private javax.swing.JScrollPane questionListScrollPane;
    private javax.swing.JPanel questionPanel;
    private javax.swing.JTextArea questionTextArea;
    private javax.swing.JButton returnToMenuButton;
    private javax.swing.JLabel title;
    private javax.swing.JButton toggleModeBtn;
    private javax.swing.JButton ubahSoalButton;
    // End of variables declaration//GEN-END:variables
}
