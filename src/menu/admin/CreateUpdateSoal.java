/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.admin;
import config.ModeChanger;
import config.Settings;

/**
 *
 * @author Ri_zki
 */
public class CreateUpdateSoal extends javax.swing.JFrame implements ModeChanger{
    
    private int index = 0;
    private String jawabanBenar;
    private String mode;

    private String getMode() {
        return mode;
    }
    private void setMode(String mode) {
        this.mode = mode;
    }
    
    
    private String getJawabanBenar() {
        return jawabanBenar;
    }
    private void setJawabanBenar(String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
        
        answerA_RadioBtn.setSelected(jawabanBenar.equals(answerA_RadioBtn.getText()));
        answerB_RadioBtn.setSelected(jawabanBenar.equals(answerB_RadioBtn.getText()));
        answerC_RadioBtn.setSelected(jawabanBenar.equals(answerC_RadioBtn.getText()));
        answerD_RadioBtn.setSelected(jawabanBenar.equals(answerD_RadioBtn.getText()));
        answerE_RadioBtn.setSelected(jawabanBenar.equals(answerE_RadioBtn.getText()));
    }
    
    /**
     * 
     */
    public CreateUpdateSoal() {
        super("Aplikasi Pendaftaran CPNS 2020");
        initComponents(); 
    }
    
    public CreateUpdateSoal(String mode) {
        super("Aplikasi Pendaftaran CPNS 2020");
        initComponents();
        changeMode();

        setMode(mode);
        this.setJawabanBenar("a");
        disableCheckLabel();  
    }
    
    public CreateUpdateSoal(String mode, int index) {
        super("Aplikasi Pendaftaran CPNS 2020");
        initComponents();
        changeMode();

        setMode(mode);
        this.index = index;
        populateForm();
        disableCheckLabel();  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        background = new javax.swing.JPanel();
        navBar = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        toggleModeBtn = new javax.swing.JButton();
        questionPanel = new javax.swing.JPanel();
        questionLabel = new javax.swing.JLabel();
        simpanButton = new javax.swing.JButton();
        returnToMenuButton = new javax.swing.JButton();
        questionTextArea = new javax.swing.JTextArea();
        questionCheckLabel = new javax.swing.JLabel();
        jawabanLabel = new javax.swing.JLabel();
        answerCheckLabelA = new javax.swing.JLabel();
        answerCheckLabelB = new javax.swing.JLabel();
        answerCheckLabelC = new javax.swing.JLabel();
        answerCheckLabelD = new javax.swing.JLabel();
        answerCheckLabelE = new javax.swing.JLabel();
        answerA_RadioBtn = new javax.swing.JRadioButton();
        answerB_RadioBtn = new javax.swing.JRadioButton();
        answerC_RadioBtn = new javax.swing.JRadioButton();
        answerD_RadioBtn = new javax.swing.JRadioButton();
        answerE_RadioBtn = new javax.swing.JRadioButton();
        answerFieldA = new javax.swing.JTextField();
        answerFieldB = new javax.swing.JTextField();
        answerFieldC = new javax.swing.JTextField();
        answerFieldD = new javax.swing.JTextField();
        answerFieldE = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(500, 350));

        background.setBackground(new java.awt.Color(230, 230, 240));
        background.setPreferredSize(new java.awt.Dimension(650, 600));

        navBar.setBackground(new java.awt.Color(0, 102, 153));
        navBar.setToolTipText("");

        title.setFont(new java.awt.Font("Avenir", 0, 20)); // NOI18N
        title.setForeground(new java.awt.Color(240, 240, 250));
        title.setText("Aplikasi Pendaftaran CPNS 2020");

        toggleModeBtn.setText("Dark Mode");
        toggleModeBtn.setEnabled(false);
        toggleModeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toggleModeBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout navBarLayout = new javax.swing.GroupLayout(navBar);
        navBar.setLayout(navBarLayout);
        navBarLayout.setHorizontalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(toggleModeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        navBarLayout.setVerticalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toggleModeBtn)
                    .addComponent(title))
                .addGap(533, 533, 533))
        );

        questionPanel.setBackground(new java.awt.Color(0, 153, 204));
        questionPanel.setEnabled(false);
        questionPanel.setPreferredSize(new java.awt.Dimension(200, 150));

        questionLabel.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        questionLabel.setForeground(new java.awt.Color(240, 240, 240));
        questionLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        questionLabel.setText("Pertanyaan");

        simpanButton.setBackground(new java.awt.Color(255, 153, 0));
        simpanButton.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        simpanButton.setText("Simpan");
        simpanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanButtonActionPerformed(evt);
            }
        });

        returnToMenuButton.setText("Cancel");
        returnToMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnToMenuButtonActionPerformed(evt);
            }
        });

        questionTextArea.setBackground(new java.awt.Color(225, 225, 225));
        questionTextArea.setColumns(20);
        questionTextArea.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        questionTextArea.setForeground(new java.awt.Color(51, 51, 51));
        questionTextArea.setLineWrap(true);
        questionTextArea.setRows(5);
        questionTextArea.setText("Object adalah instance dari class. Dari class Fruit object apakah yang mungkin dibuat?");
        questionTextArea.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(240, 240, 240), 2, true));

        questionCheckLabel.setBackground(new java.awt.Color(244, 66, 54));
        questionCheckLabel.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        questionCheckLabel.setForeground(new java.awt.Color(240, 240, 240));
        questionCheckLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        questionCheckLabel.setText("Pertanyaan wajib diisi");
        questionCheckLabel.setOpaque(true);

        jawabanLabel.setFont(new java.awt.Font("Avenir", 0, 16)); // NOI18N
        jawabanLabel.setForeground(new java.awt.Color(240, 240, 240));
        jawabanLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jawabanLabel.setText("Opsi Jawaban (Klik pada huruf untuk memilih Jawaban benar)");

        answerCheckLabelA.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        answerCheckLabelA.setForeground(new java.awt.Color(240, 240, 240));
        answerCheckLabelA.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        answerCheckLabelA.setText("Jawaban tidak boleh kosong");

        answerCheckLabelB.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        answerCheckLabelB.setForeground(new java.awt.Color(240, 240, 240));
        answerCheckLabelB.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        answerCheckLabelB.setText("Jawaban tidak boleh kosong");

        answerCheckLabelC.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        answerCheckLabelC.setForeground(new java.awt.Color(240, 240, 240));
        answerCheckLabelC.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        answerCheckLabelC.setText("Jawaban tidak boleh kosong");

        answerCheckLabelD.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        answerCheckLabelD.setForeground(new java.awt.Color(240, 240, 240));
        answerCheckLabelD.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        answerCheckLabelD.setText("Jawaban tidak boleh kosong");

        answerCheckLabelE.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        answerCheckLabelE.setForeground(new java.awt.Color(240, 240, 240));
        answerCheckLabelE.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        answerCheckLabelE.setText("Jawaban tidak boleh kosong");

        answerA_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerA_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerA_RadioBtn.setSelected(true);
        answerA_RadioBtn.setText("a");
        answerA_RadioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                answerA_RadioBtnActionPerformed(evt);
            }
        });

        answerB_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerB_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerB_RadioBtn.setText("b");
        answerB_RadioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                answerB_RadioBtnActionPerformed(evt);
            }
        });

        answerC_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerC_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerC_RadioBtn.setText("c");
        answerC_RadioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                answerC_RadioBtnActionPerformed(evt);
            }
        });

        answerD_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerD_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerD_RadioBtn.setText("d");
        answerD_RadioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                answerD_RadioBtnActionPerformed(evt);
            }
        });

        answerE_RadioBtn.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        answerE_RadioBtn.setForeground(new java.awt.Color(240, 240, 240));
        answerE_RadioBtn.setText("e");
        answerE_RadioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                answerE_RadioBtnActionPerformed(evt);
            }
        });

        answerFieldA.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        answerFieldB.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        answerFieldC.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        answerFieldD.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        answerFieldE.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout questionPanelLayout = new javax.swing.GroupLayout(questionPanel);
        questionPanel.setLayout(questionPanelLayout);
        questionPanelLayout.setHorizontalGroup(
            questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(simpanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addGap(328, 328, 328)
                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(answerCheckLabelA)
                            .addComponent(answerCheckLabelC)
                            .addComponent(answerCheckLabelB)
                            .addComponent(answerCheckLabelD)
                            .addComponent(answerCheckLabelE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(questionPanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(questionLabel)
                            .addComponent(jawabanLabel)
                            .addGroup(questionPanelLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(questionTextArea, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(answerA_RadioBtn)
                                    .addComponent(answerB_RadioBtn)
                                    .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(answerC_RadioBtn)
                                            .addGroup(questionPanelLayout.createSequentialGroup()
                                                .addGap(40, 40, 40)
                                                .addComponent(answerFieldC, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(questionPanelLayout.createSequentialGroup()
                                            .addGap(40, 40, 40)
                                            .addComponent(answerFieldB, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(answerD_RadioBtn)
                                            .addGroup(questionPanelLayout.createSequentialGroup()
                                                .addGap(40, 40, 40)
                                                .addComponent(answerFieldD, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(answerE_RadioBtn)
                                            .addGroup(questionPanelLayout.createSequentialGroup()
                                                .addGap(40, 40, 40)
                                                .addComponent(answerFieldE, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(questionCheckLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 19, Short.MAX_VALUE))
                    .addGroup(questionPanelLayout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(answerFieldA, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        questionPanelLayout.setVerticalGroup(
            questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(questionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(questionTextArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(questionCheckLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jawabanLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(answerFieldA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(answerA_RadioBtn)
                    .addComponent(answerCheckLabelA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(answerCheckLabelB)
                    .addComponent(answerFieldB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(answerB_RadioBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(answerFieldC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(answerC_RadioBtn)
                    .addComponent(answerCheckLabelC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(answerFieldD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(answerD_RadioBtn)
                    .addComponent(answerCheckLabelD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(answerFieldE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(answerE_RadioBtn)
                    .addComponent(answerCheckLabelE))
                .addGap(41, 41, 41)
                .addGroup(questionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(simpanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49))
        );

        javax.swing.GroupLayout backgroundLayout = new javax.swing.GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(navBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(questionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        backgroundLayout.setVerticalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addComponent(navBar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(questionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void changeMode() {                                            
        if ( Settings.isDarkModeSelected() ) { 
            toggleModeBtn.setText("Light Mode");
            navBar.setBackground(new java.awt.Color(0,51,102));
            questionPanel.setBackground(new java.awt.Color(0,102,153));
            background.setBackground(new java.awt.Color(30, 30, 30));
            
        } else { 
            toggleModeBtn.setText("Dark Mode");
            navBar.setBackground(new java.awt.Color(0,102,153));
            questionPanel.setBackground(new java.awt.Color(0,153, 204));
            background.setBackground(new java.awt.Color(230, 230, 240));
        }
    }
    
    private void disableCheckLabel(){
        questionCheckLabel.setBackground( Settings.isDarkModeSelected() ? 
                new java.awt.Color(0,102,153) : 
                new java.awt.Color(0,153, 204));
        questionCheckLabel.setForeground( Settings.isDarkModeSelected() ? 
                new java.awt.Color(0,102,153) : 
                new java.awt.Color(0,153, 204));
        answerCheckLabelA.setVisible(false);
        answerCheckLabelB.setVisible(false);
        answerCheckLabelC.setVisible(false);
        answerCheckLabelD.setVisible(false);
        answerCheckLabelE.setVisible(false);
    }
    
    private void enableQuestionCheckLabel(){
        questionCheckLabel.setBackground(new java.awt.Color(255, 0, 50));
        questionCheckLabel.setForeground(new java.awt.Color(240, 240, 240));
    }

    
    
    private void populateForm(){
        questionLabel.setText("Ubah Soal " + (index+1)+ " dari " + data.DataSoal.getArraySoal().size());
        questionTextArea.setText(data.DataSoal.getArrayIndex(index).getQuestion());
        answerFieldA.setText(data.DataSoal.getArrayIndex(index).getJawaban_A());
        answerFieldB.setText(data.DataSoal.getArrayIndex(index).getJawaban_B());
        answerFieldC.setText(data.DataSoal.getArrayIndex(index).getJawaban_C());
        answerFieldD.setText(data.DataSoal.getArrayIndex(index).getJawaban_D());
        answerFieldE.setText(data.DataSoal.getArrayIndex(index).getJawaban_E());
        
        this.setJawabanBenar( data.DataSoal.getArrayIndex(index).getJawabanBenar() );
        
        answerA_RadioBtn.setSelected(getJawabanBenar().equals("a"));
        answerB_RadioBtn.setSelected(getJawabanBenar().equals("b"));
        answerC_RadioBtn.setSelected(getJawabanBenar().equals("c"));
        answerD_RadioBtn.setSelected(getJawabanBenar().equals("d"));
        answerE_RadioBtn.setSelected(getJawabanBenar().equals("e"));
    }
    
    
    //Cek field pertanyaan/jawaban terisi dengan benar/tidak
    private boolean isFormFilledCorrectly(){
        if(questionTextArea.getText().isBlank()){
            enableQuestionCheckLabel();
        }
        answerCheckLabelA.setVisible(answerFieldA.getText().isBlank());
        answerCheckLabelB.setVisible(answerFieldB.getText().isBlank());
        answerCheckLabelC.setVisible(answerFieldC.getText().isBlank());
        answerCheckLabelD.setVisible(answerFieldD.getText().isBlank());
        answerCheckLabelE.setVisible(answerFieldE.getText().isBlank());

        return !questionTextArea.getText().isBlank() && 
                !answerFieldA.getText().isBlank() &&
                !answerFieldB.getText().isBlank() &&
                !answerFieldC.getText().isBlank() &&
                !answerE_RadioBtn.getText().isBlank() &&
                !answerFieldE.getText().isBlank();
    }
    
    
    
    
    private void toggleModeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleModeBtnActionPerformed
        Settings.changeToggleMode();
        changeMode();
    }//GEN-LAST:event_toggleModeBtnActionPerformed

    private void returnToMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnToMenuButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_returnToMenuButtonActionPerformed

    private void simpanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanButtonActionPerformed
        if(isFormFilledCorrectly()){
            switch(getMode()){
                case "tambah":
                    /*
                    data.DataSoal.tambahData( 
                        questionTextArea.getText(),
                        answerFieldA.getText(),
                        answerFieldB.getText(),
                        answerFieldC.getText(), 
                        answerFieldD.getText(), 
                        answerFieldE.getText(), 
                        getJawabanBenar() );
                    */
                    
                    new data.SoalFileManagement().insert( "soal.txt",
                        questionTextArea.getText(),
                        answerFieldA.getText(),
                        answerFieldB.getText(),
                        answerFieldC.getText(), 
                        answerFieldD.getText(), 
                        answerFieldE.getText(), 
                        getJawabanBenar() );
                    float nilai = (float) (100.0 / data.DataSoal.getArraySoal().size());

                    data.DataSoal.getArraySoal().forEach((i) -> {
                            i.setNilai(nilai);
                    });
                    break;

                case "ubah":
                    data.DataSoal.updateData(index,
                        questionTextArea.getText(),
                        answerFieldA.getText(),
                        answerFieldB.getText(),
                        answerFieldC.getText(), 
                        answerFieldD.getText(), 
                        answerFieldE.getText(), 
                        getJawabanBenar() );
                    break;    
            }
            
            javax.swing.JOptionPane.showMessageDialog(
                    null, 
                    (getMode().equals("tambah")) ? "Penambahan data sukses" :
                    (getMode().equals("ubah")) ? "Perubahan data sukses"  : "DONE", 
                    "Manajemen Soal Tes", 
                    javax.swing.JOptionPane.INFORMATION_MESSAGE); 
            this.dispose();
        }
    }//GEN-LAST:event_simpanButtonActionPerformed

    private void answerA_RadioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_answerA_RadioBtnActionPerformed
        this.setJawabanBenar(answerA_RadioBtn.getText());
    }//GEN-LAST:event_answerA_RadioBtnActionPerformed

    private void answerB_RadioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_answerB_RadioBtnActionPerformed
        this.setJawabanBenar(answerB_RadioBtn.getText());
    }//GEN-LAST:event_answerB_RadioBtnActionPerformed

    private void answerC_RadioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_answerC_RadioBtnActionPerformed
        this.setJawabanBenar(answerC_RadioBtn.getText());
    }//GEN-LAST:event_answerC_RadioBtnActionPerformed

    private void answerD_RadioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_answerD_RadioBtnActionPerformed
        this.setJawabanBenar(answerD_RadioBtn.getText());
    }//GEN-LAST:event_answerD_RadioBtnActionPerformed

    private void answerE_RadioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_answerE_RadioBtnActionPerformed
        this.setJawabanBenar(answerE_RadioBtn.getText());
    }//GEN-LAST:event_answerE_RadioBtnActionPerformed


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CreateUpdateSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CreateUpdateSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CreateUpdateSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CreateUpdateSoal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CreateUpdateSoal().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton answerA_RadioBtn;
    private javax.swing.JRadioButton answerB_RadioBtn;
    private javax.swing.JRadioButton answerC_RadioBtn;
    private javax.swing.JLabel answerCheckLabelA;
    private javax.swing.JLabel answerCheckLabelB;
    private javax.swing.JLabel answerCheckLabelC;
    private javax.swing.JLabel answerCheckLabelD;
    private javax.swing.JLabel answerCheckLabelE;
    private javax.swing.JRadioButton answerD_RadioBtn;
    private javax.swing.JRadioButton answerE_RadioBtn;
    private javax.swing.JTextField answerFieldA;
    private javax.swing.JTextField answerFieldB;
    private javax.swing.JTextField answerFieldC;
    private javax.swing.JTextField answerFieldD;
    private javax.swing.JTextField answerFieldE;
    private javax.swing.JPanel background;
    private javax.swing.JLabel jawabanLabel;
    private javax.swing.JPanel navBar;
    private javax.swing.JLabel questionCheckLabel;
    private javax.swing.JLabel questionLabel;
    private javax.swing.JPanel questionPanel;
    private javax.swing.JTextArea questionTextArea;
    private javax.swing.JButton returnToMenuButton;
    private javax.swing.JButton simpanButton;
    private javax.swing.JLabel title;
    private javax.swing.JButton toggleModeBtn;
    // End of variables declaration//GEN-END:variables
}
