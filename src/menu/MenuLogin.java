/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import config.ModeChanger;
import menu.pendaftar.MenuPendaftar;
import menu.admin.MenuAdmin;
import menu.akun.MenuDaftarAkun;
import menu.akun.MenuLupaPassword;
import config.Settings;
import data.DataPendaftar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import config.TestCpnsConfig;

/**
 *
 * @author Ri_zki
 */
public final class MenuLogin extends javax.swing.JFrame implements ModeChanger{
    private int kesempatan_login = projekakhirswing.ProjekAkhirSwing.getKesempatan_login();
    
    private String loginAs;
    private boolean usernameLabelClicked = false;
    private boolean notificationEnabled = false;

    public String getLoginAs() {
        return loginAs;
    }
    public void setLoginAs(String loginAs) {
        this.loginAs = loginAs;
    }

    private boolean isUsernameLabelClicked() {
        return usernameLabelClicked;
    }
    private void setUsernameLabelClicked(boolean usernameLabelClicked) {
        this.usernameLabelClicked = usernameLabelClicked;
    }

    public boolean isNotificationEnabled() {
        return notificationEnabled;
    }
    public void setNotificationEnabled(boolean notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
    }
    
    /**
     * Creates new form MenuLogin
     */
    public MenuLogin() {
        initComponents();
        changeMode();
    }
    
    public MenuLogin(String loginAs) {
        super("Aplikasi Pendaftaran CPNS 2020");
        initComponents();
        changeMode();
        hideNotifPanel();
        
        this.setLoginAs(loginAs);
        setPendaftarOptionsVisibiity(
                ( loginAs.equals("admin"))      ? false :
                ( loginAs.equals("pendaftar"))  ? true : true
        );
        
        if( TestCpnsConfig.isBukaTes() ){
            this.noAccountLabel.setVisible(false);
            this.CreateAccountLabel.setVisible(false);
        }
        
        usernameLabel.setText(
                ( loginAs.equals("admin"))      ? "Username" :
                ( loginAs.equals("pendaftar"))  ? "NIK" : "Username"
        
        );
        
        usernameField.setText(
                ( loginAs.equals("admin"))      ? "Username" :
                ( loginAs.equals("pendaftar"))  ? "NIK" : "Username"
        
        );
        
        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource(
                ( loginAs.equals("admin"))      ?   "/resources/logo/admin.png" :
                ( loginAs.equals("pendaftar"))  ?   "/resources/logo/user.png" : 
                                                    "/resources/logo/user.png"
        )));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        background = new javax.swing.JPanel();
        navBar = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        toggleModeBtn = new javax.swing.JButton();
        loginPanel = new javax.swing.JPanel();
        notificationPanel = new javax.swing.JPanel();
        notificationLabel = new javax.swing.JLabel();
        loginInnerPanel = new javax.swing.JPanel();
        usernameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        usernameField = new javax.swing.JTextField();
        passwordField = new javax.swing.JPasswordField();
        loginButton = new javax.swing.JButton();
        returnToMenuButton = new javax.swing.JButton();
        noAccountLabel = new javax.swing.JLabel();
        passwordForgotLabel = new javax.swing.JLabel();
        CreateAccountLabel = new javax.swing.JLabel();
        showPasswordBtn = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(500, 350));

        background.setBackground(new java.awt.Color(230, 230, 240));
        background.setPreferredSize(new java.awt.Dimension(650, 400));

        navBar.setBackground(new java.awt.Color(0, 153, 204));

        title.setFont(new java.awt.Font("Avenir", 0, 20)); // NOI18N
        title.setForeground(new java.awt.Color(240, 240, 250));
        title.setText("Aplikasi Pendaftaran CPNS 2020");

        toggleModeBtn.setText("Dark Mode");
        toggleModeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toggleModeBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout navBarLayout = new javax.swing.GroupLayout(navBar);
        navBar.setLayout(navBarLayout);
        navBarLayout.setHorizontalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
                .addComponent(toggleModeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        navBarLayout.setVerticalGroup(
            navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, navBarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(navBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toggleModeBtn)
                    .addComponent(title))
                .addContainerGap())
        );

        loginPanel.setBackground(new java.awt.Color(0, 153, 204));
        loginPanel.setPreferredSize(new java.awt.Dimension(200, 150));

        notificationPanel.setBackground(new java.awt.Color(244, 64, 54));

        notificationLabel.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        notificationLabel.setForeground(new java.awt.Color(240, 240, 240));
        notificationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        notificationLabel.setText("Username / Password Salah");

        javax.swing.GroupLayout notificationPanelLayout = new javax.swing.GroupLayout(notificationPanel);
        notificationPanel.setLayout(notificationPanelLayout);
        notificationPanelLayout.setHorizontalGroup(
            notificationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, notificationPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(notificationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );
        notificationPanelLayout.setVerticalGroup(
            notificationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, notificationPanelLayout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addComponent(notificationLabel)
                .addContainerGap())
        );

        loginInnerPanel.setBackground(new java.awt.Color(0, 153, 204));
        loginInnerPanel.setPreferredSize(new java.awt.Dimension(200, 150));

        usernameLabel.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        usernameLabel.setForeground(new java.awt.Color(240, 240, 240));
        usernameLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        usernameLabel.setText("Username");

        passwordLabel.setFont(new java.awt.Font("Avenir", 0, 18)); // NOI18N
        passwordLabel.setForeground(new java.awt.Color(240, 240, 240));
        passwordLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        passwordLabel.setText("Password");

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/logo/user.png"))); // NOI18N

        usernameField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        usernameField.setText("Username");
        usernameField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                usernameFieldMouseClicked(evt);
            }
        });

        loginButton.setBackground(new java.awt.Color(255, 153, 0));
        loginButton.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        loginButton.setText("Login");
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });

        returnToMenuButton.setText("Kembali Ke Menu");
        returnToMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnToMenuButtonActionPerformed(evt);
            }
        });

        noAccountLabel.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        noAccountLabel.setForeground(new java.awt.Color(240, 240, 240));
        noAccountLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        noAccountLabel.setText("Belum punya akun?");

        passwordForgotLabel.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        passwordForgotLabel.setForeground(new java.awt.Color(204, 255, 204));
        passwordForgotLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        passwordForgotLabel.setText("Lupa Password?");
        passwordForgotLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        passwordForgotLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                passwordForgotLabelMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                passwordForgotLabelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                passwordForgotLabelMouseReleased(evt);
            }
        });

        CreateAccountLabel.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        CreateAccountLabel.setForeground(new java.awt.Color(204, 255, 204));
        CreateAccountLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        CreateAccountLabel.setText("Buat akun baru");
        CreateAccountLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        CreateAccountLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CreateAccountLabelMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CreateAccountLabelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                CreateAccountLabelMouseReleased(evt);
            }
        });

        showPasswordBtn.setText("Show");
        showPasswordBtn.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                showPasswordBtnItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout loginInnerPanelLayout = new javax.swing.GroupLayout(loginInnerPanel);
        loginInnerPanel.setLayout(loginInnerPanelLayout);
        loginInnerPanelLayout.setHorizontalGroup(
            loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginInnerPanelLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(logo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(loginInnerPanelLayout.createSequentialGroup()
                        .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(loginInnerPanelLayout.createSequentialGroup()
                                .addComponent(noAccountLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CreateAccountLabel))
                            .addComponent(passwordForgotLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(passwordLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(usernameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(62, Short.MAX_VALUE))
                    .addGroup(loginInnerPanelLayout.createSequentialGroup()
                        .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(loginInnerPanelLayout.createSequentialGroup()
                                .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(showPasswordBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, loginInnerPanelLayout.createSequentialGroup()
                                    .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 31, Short.MAX_VALUE))))
        );
        loginInnerPanelLayout.setVerticalGroup(
            loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginInnerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(loginInnerPanelLayout.createSequentialGroup()
                        .addComponent(usernameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(passwordLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPasswordBtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(passwordForgotLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(returnToMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(logo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(loginInnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(noAccountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CreateAccountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout loginPanelLayout = new javax.swing.GroupLayout(loginPanel);
        loginPanel.setLayout(loginPanelLayout);
        loginPanelLayout.setHorizontalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(notificationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(loginInnerPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        loginPanelLayout.setVerticalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginPanelLayout.createSequentialGroup()
                .addComponent(notificationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loginInnerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout backgroundLayout = new javax.swing.GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(navBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(loginPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
        );
        backgroundLayout.setVerticalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addComponent(navBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(loginPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void changeMode() {                                            
        if ( Settings.isDarkModeSelected() ) { 
            toggleModeBtn.setText("Light Mode");
            navBar.setBackground(new java.awt.Color(0,51,102));
            
            loginPanel.setBackground(new java.awt.Color(0,102,153));
            loginInnerPanel.setBackground(new java.awt.Color(0,102,153));
            background.setBackground(new java.awt.Color(30, 30, 30));
            
        } else { 
            toggleModeBtn.setText("Dark Mode");
            navBar.setBackground(new java.awt.Color(0,102,153));
            
            loginPanel.setBackground(new java.awt.Color(0,153, 204));
            loginInnerPanel.setBackground(new java.awt.Color(0,153, 204));
            background.setBackground(new java.awt.Color(230, 230, 240));
        }
        if( !isNotificationEnabled()){
            hideNotifPanel();
        }
        
    }
    
    private void hideNotifPanel() {                                            
        if ( Settings.isDarkModeSelected() ) { 
            notificationLabel.setForeground(new java.awt.Color(0,102,153));
            notificationPanel.setBackground(new java.awt.Color(0,102,153));
        } else { 
            notificationLabel.setForeground(new java.awt.Color(0,153, 204));
            notificationPanel.setBackground(new java.awt.Color(0,153, 204));
        }
    }
    
    private void showNotifPanel(String message) {                                            
        setNotificationEnabled(true);
        
        notificationLabel.setText(message);
        notificationLabel.setForeground(new java.awt.Color(240, 240, 241));
        notificationPanel.setBackground(new java.awt.Color(255,0,51));
    }
   
    
    
    private void setPendaftarOptionsVisibiity(boolean state){
        this.passwordForgotLabel.setVisible(state);
        this.noAccountLabel.setVisible(state);
        this.CreateAccountLabel.setVisible(state);
    }


    
    private void usernameFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usernameFieldMouseClicked
        if( !isUsernameLabelClicked() ){
            setUsernameLabelClicked(true);
            usernameField.setText("");
        }
        
    }//GEN-LAST:event_usernameFieldMouseClicked

    private void checkLogin(String username, String password){
        if(getLoginAs().equals("admin")){
            if( username.equals("admin") && password.equals("admin") ){
                projekakhirswing.ProjekAkhirSwing.setKesempatan_login(kesempatan_login);
                JOptionPane.showMessageDialog(
                        null, 
                        "Selamat datang, admin",
                        "Pesan Login", 
                        JOptionPane.INFORMATION_MESSAGE);
                
                MenuAdmin menuAdmin = new MenuAdmin();
                menuAdmin.setVisible(true);
                menuAdmin.setLocationRelativeTo(null);
                this.dispose();
            }else {
                kesempatan_login -= 1;
                projekakhirswing.ProjekAkhirSwing.setKesempatan_login(kesempatan_login);
                showNotifPanel("Username / Password Salah. Kesempatan login: " + kesempatan_login);
            }
            if( kesempatan_login == 0 ){
                projekakhirswing.ProjekAkhirSwing.setKesempatan_login(kesempatan_login);
                JOptionPane.showMessageDialog(
                        null, 
                        "Anda tidak dapat login sebagai admin",
                        "Pesan Login", 
                        JOptionPane.ERROR_MESSAGE); 
                returnToMenuAwal();
            }
        }else{
            boolean loginStatus = false;
            int indeksPendaftar = 0;
            
            for (int i=0; i < DataPendaftar.getArrayPendaftar().size(); i++){
                if ( username.equals( DataPendaftar.getArrayIndex(i).getNik() ) && 
                    DataPendaftar.getArrayIndex(i).getPassword().equals( password )) {
                    indeksPendaftar = i;
                    loginStatus = true;
                }
            }

            if( loginStatus == true ){
                JOptionPane.showMessageDialog(
                        null, 
                        "Login berhasil",
                        "Pesan Login", 
                        JOptionPane.INFORMATION_MESSAGE); 
                MenuPendaftar menuAkun = new MenuPendaftar(indeksPendaftar);
                menuAkun.setVisible(true);
                menuAkun.setLocationRelativeTo(this);
                this.dispose();
            } else {
                showNotifPanel("NIK / Password Salah");
            }
        }
    }
    
    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginButtonActionPerformed
        try {
            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
            Thread.sleep(500);
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            checkLogin(usernameField.getText(), passwordField.getText());
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_loginButtonActionPerformed

    private void returnToMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnToMenuButtonActionPerformed
        MenuAwal.returnToMenuAwal();
        this.dispose();
    }//GEN-LAST:event_returnToMenuButtonActionPerformed
    
    
    private void passwordForgotLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_passwordForgotLabelMouseClicked
        MenuLupaPassword menuPassForgot = new MenuLupaPassword();
        menuPassForgot.setVisible(true);
        menuPassForgot.setLocationRelativeTo(null);
        this.dispose();
    }//GEN-LAST:event_passwordForgotLabelMouseClicked

    private void CreateAccountLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreateAccountLabelMouseClicked
        MenuDaftarAkun menuDaftar = new MenuDaftarAkun();
        menuDaftar.setVisible(true);
        menuDaftar.setLocationRelativeTo(null);
        this.dispose();
    }//GEN-LAST:event_CreateAccountLabelMouseClicked

    private void toggleModeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleModeBtnActionPerformed
        Settings.changeToggleMode();
        changeMode();
    }//GEN-LAST:event_toggleModeBtnActionPerformed

    private void showPasswordBtnItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_showPasswordBtnItemStateChanged
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED){
            showPasswordBtn.setText("Hide");
            passwordField.setEchoChar( (char) 0 );
        } else {
            showPasswordBtn.setText("Show");
            passwordField.setEchoChar( '*' );
        }
    }//GEN-LAST:event_showPasswordBtnItemStateChanged

    private void CreateAccountLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreateAccountLabelMousePressed
        CreateAccountLabel.setForeground(new java.awt.Color(153, 204, 255));
    }//GEN-LAST:event_CreateAccountLabelMousePressed

    private void CreateAccountLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreateAccountLabelMouseReleased
        CreateAccountLabel.setForeground(new java.awt.Color(204, 255, 204));
    }//GEN-LAST:event_CreateAccountLabelMouseReleased

    private void passwordForgotLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_passwordForgotLabelMousePressed
        passwordForgotLabel.setForeground(new java.awt.Color(153, 204, 255));
    }//GEN-LAST:event_passwordForgotLabelMousePressed

    private void passwordForgotLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_passwordForgotLabelMouseReleased
        passwordForgotLabel.setForeground(new java.awt.Color(204, 255, 204));
    }//GEN-LAST:event_passwordForgotLabelMouseReleased

    private void returnToMenuAwal(){
        menu.MenuAwal.returnToMenuAwal();
        this.dispose();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CreateAccountLabel;
    private javax.swing.JPanel background;
    private javax.swing.JButton loginButton;
    private javax.swing.JPanel loginInnerPanel;
    private javax.swing.JPanel loginPanel;
    private javax.swing.JLabel logo;
    private javax.swing.JPanel navBar;
    private javax.swing.JLabel noAccountLabel;
    private javax.swing.JLabel notificationLabel;
    private javax.swing.JPanel notificationPanel;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordForgotLabel;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JButton returnToMenuButton;
    private javax.swing.JToggleButton showPasswordBtn;
    private javax.swing.JLabel title;
    private javax.swing.JButton toggleModeBtn;
    private javax.swing.JTextField usernameField;
    private javax.swing.JLabel usernameLabel;
    // End of variables declaration//GEN-END:variables
}
