package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Ri_zki
 */
public class PendaftarFileManagement {
    public void insert(String fileName, 
            String nama, 
            String nik, 
            String no_kk, 
            int tanggal, 
            int bulan, 
            int tahun, 
            String password)
    {
        try{
            FileWriter f = new FileWriter(fileName ,true);
            f.write(nama + "\r\n");
            f.write(nik + "\r\n");
            f.write(no_kk + "\r\n");
            f.write(tanggal + "\r\n");
            f.write(bulan + "\r\n");
            f.write(tahun + "\r\n");
            f.write(password + "\r\n");
            f.close();
            
            data.DataPendaftar.tambahData( 
                    nama,
                    nik,
                    no_kk,
                    tanggal,
                    bulan,
                    tahun,
                    password);
                
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    
    public void readFile(String fileName, boolean append){
        try{
            File f = new File(fileName);
            Scanner read = new Scanner(f);
 
            while(read.hasNextLine()){

                String nama = read.nextLine();
                String nik = read.nextLine();
                String no_kk = read.nextLine();
                String tanggal = read.nextLine();
                String bulan = read.nextLine();
                String tahun = read.nextLine();
                String password = read.nextLine();
                
                if(append){
                    data.DataPendaftar.tambahData( 
                            nama,
                            nik,
                            no_kk,
                            Integer.parseInt(tanggal),
                            Integer.parseInt(bulan),
                            Integer.parseInt(tahun),
                            password);
                }
            }

        }catch(IOException e){
           e.printStackTrace();
        }
    }
    
    
    public void delete(String fileName){
        try{
            FileWriter f = new FileWriter(fileName ,false);
            f.write("");
            f.close();

            System.out.println("Data Telah Dihapus");
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
