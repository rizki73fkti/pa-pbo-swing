package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Ri_zki
 */
public class SoalFileManagement {
    public void insert(
            String fileName,
            String soal,
            String jawabA,
            String jawabB,
            String jawabC,
            String jawabD,
            String jawabE,
            String jawabBenar)
    {
        try{
            FileWriter f = new FileWriter(fileName ,true);
            f.write(soal + "\r\n");
            f.write(jawabA + "\r\n");
            f.write(jawabB + "\r\n");
            f.write(jawabC + "\r\n");
            f.write(jawabD + "\r\n");
            f.write(jawabE + "\r\n");
            f.write(jawabBenar + "\r\n");
            f.close();
            
            data.DataSoal.tambahData( 
                    soal,
                    jawabA,
                    jawabB,
                    jawabC,
                    jawabD,
                    jawabE,
                    jawabBenar);
                
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    
    public void readFile(String fileName, boolean append){
        try{
            File f = new File(fileName);
            Scanner read = new Scanner(f);
            
            while(read.hasNextLine()){

                String soal = read.nextLine().trim();
                String jawabA = read.nextLine();
                String jawabB = read.nextLine();
                String jawabC = read.nextLine();
                String jawabD = read.nextLine();
                String jawabE = read.nextLine();
                String jawabBenar = read.nextLine();    

                if(append){
                    data.DataSoal.tambahData( 
                        soal,
                        jawabA,
                        jawabB,
                        jawabC,
                        jawabD,
                        jawabE,
                        jawabBenar);
                }
            }

            float nilai = (float) (100.0 / data.DataSoal.getArraySoal().size());

            data.DataSoal.getArraySoal().forEach((i) -> {
                    i.setNilai(nilai);
            });
        }catch(IOException e){
           e.printStackTrace();
        }
    }
    
    
    public void delete(String fileName){
        try{
            FileWriter f = new FileWriter(fileName ,false);
            f.write("");
            f.close();

            System.out.println("Data Telah Dihapus");
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
