package data;
import java.util.ArrayList;

public class DataPendaftar{
    private static ArrayList<Pendaftar> arrayPendaftar = new ArrayList<>();
    private static int indeks_hapusData;

    public static void tambahData(
            String nama, String nik, String no_kk, 
            int tanggal, int bulan, int tahun, 
            String password) 
    {    
        arrayPendaftar.add( new Pendaftar(
                nama, nik,  no_kk, 
                tanggal,  bulan,  tahun, 
                password
                )
        
        );
        new DataJawabanPendaftar().tambahData();
    }

    
    public static ArrayList<Pendaftar> getArrayPendaftar() {
        return arrayPendaftar;
    }
    
    public static Pendaftar getArrayIndex(int i) {
        return arrayPendaftar.get(i);
    }
    
    public static void updateData(int indeks,
            String nama, String nik, String no_kk, 
            int tanggal, int bulan, int tahun,
            String pengubah) {
        getArrayIndex(indeks).setNama( nama );
        getArrayIndex(indeks).setNik( nik);
        getArrayIndex(indeks).setNoKk( no_kk);
        getArrayIndex(indeks).setTanggal( tanggal);
        getArrayIndex(indeks).setBulan( bulan);
        getArrayIndex(indeks).setTahun( tahun);
        
        if(pengubah.equals("pendaftar")){
            getArrayIndex(indeks).setKesempatan_ubah_data(0);
        }
    }

     public void hapusData(int index) {
        getArrayPendaftar().remove(index);
        setIndeks_hapusData(index);
        DataJawabanPendaftar dataJawaban = new DataJawabanPendaftar();
        dataJawaban.hapusData();
    }

    public static int getIndeks_hapusData() {
        return indeks_hapusData;
    }

    public void setIndeks_hapusData(int indeks_hapusData) {
        DataPendaftar.indeks_hapusData = indeks_hapusData;
    }
}
