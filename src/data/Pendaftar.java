package data;
public class Pendaftar {
    
    private boolean telahIkutTes;
    private String nik, noKk, nama, password;
    private int tanggal, bulan, tahun,
            kesempatan_ubah_data;

    public Pendaftar(
            String nama, String nik, String noKk, 
            int tanggal, int bulan, int tahun, 
            String password) {
        
        this.nama = nama;
        this.nik = nik;
        this.noKk = noKk;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
        this.password = password;
        this.kesempatan_ubah_data = 1;
        this.telahIkutTes = false;
    }

    
    
    
    //SETTER
    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public void setNoKk(String noKk) {
        this.noKk = noKk;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public void setBulan(int bulan) {
        this.bulan = bulan;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setKesempatan_ubah_data(int kesempatan_ubah_data) {
        this.kesempatan_ubah_data = kesempatan_ubah_data;
    }
    
    public void setTelahIkutTes(boolean telahIkutTes) {
        this.telahIkutTes = telahIkutTes;
    }
    
    
    
    //GETTER
    public String getNama() {
        return nama;
    }

    public String getNik() {
        return nik;
    }

    public String getNoKk() {
        return noKk;
    }

    public int getTanggal() {
        return tanggal;
    }

    public int getBulan() {
        return bulan;
    }

    public int getTahun() {
        return tahun;
    }

    public String getPassword() {
        return password;
    }

    public int getKesempatan_ubah_data() {
        return kesempatan_ubah_data;
    }
    
    public boolean isTelahIkutTes() {
        return telahIkutTes;
    }
    
}
