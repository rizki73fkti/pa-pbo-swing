package data;

import java.util.ArrayList;
import javax.swing.JOptionPane;


import soal.Soal;


public class DataSoal{
    
    private static ArrayList<Soal> arraySoal = new ArrayList<>();

    public static ArrayList<Soal> getArraySoal() {
        return arraySoal;
    }
    
    public static Soal getArrayIndex(int i) {
        return arraySoal.get(i);
    }
    
    public static void tambahData(
            String question, String jawaban_A, String jawaban_B, 
            String jawaban_C, String jawaban_D, String jawaban_E, 
            String jawabanBenar) 
    {    
        arraySoal.add( new Soal(
                question, jawaban_A, jawaban_B, 
                jawaban_C, jawaban_D, jawaban_E, 
                jawabanBenar
                )
        );
        
        float nilai = (float) (100.0 / getArraySoal().size());
        for (int i = 0; i < getArraySoal().size()   ; i++){
            getArrayIndex(i).setNilai(nilai);                    
        }
    }

    public void lihatData() {
        if ( getArraySoal().isEmpty() ){
            JOptionPane.showMessageDialog(null, "Data Soal Kosong", "Daftar Soal Tes", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    
    
    public static void updateData(int indeks,
            String question, 
            String jawaban_A, 
            String jawaban_B, 
            String jawaban_C, 
            String jawaban_D, 
            String jawaban_E, 
            String jawabanBenar) {
        getArrayIndex(indeks).setQuestion( question );
        getArrayIndex(indeks).setJawaban_A( jawaban_A );
        getArrayIndex(indeks).setJawaban_B( jawaban_B );
        getArrayIndex(indeks).setJawaban_C( jawaban_C );
        getArrayIndex(indeks).setJawaban_D( jawaban_D );
        getArrayIndex(indeks).setJawaban_E( jawaban_E );
        getArrayIndex(indeks).setJawabanBenar( jawabanBenar );
    }
    
    public static void hapusData(int indeks) {            
        getArraySoal().remove(indeks);
        if (!getArraySoal().isEmpty()){
            float nilai = (float) (100.0 / getArraySoal().size());
            for (int i = 0; i < getArraySoal().size(); i++){
                getArrayIndex(i).setNilai(nilai);                    
            }
        } else {
            config.TestCpnsConfig.setBukaTes(false);
        }
    }

}