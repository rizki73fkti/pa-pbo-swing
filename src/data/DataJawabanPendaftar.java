package data;

import soal.JawabanPendaftar;
import java.util.ArrayList;


public class DataJawabanPendaftar{
    
    private static ArrayList<JawabanPendaftar> arrayJawaban = new ArrayList<>();
    
    DataSoal dataSoal = new DataSoal();
    
    public void tambahData() {
        arrayJawaban.add(new JawabanPendaftar() );
    }
    
    public static ArrayList<JawabanPendaftar> getArrayJawaban() {
        return arrayJawaban;
    }
    
    public static JawabanPendaftar getArrayIndex(int i) {
        return arrayJawaban.get(i);
    }
    

    public static void updateData(int indeks, ArrayList<String> jawaban){
        
        getArrayIndex(indeks).setJawaban(jawaban);
        
        float nilai = 0;
        for ( int i = 0; i < DataSoal.getArraySoal().size(); i++){
            if ( getArrayIndex(indeks).getJawabanIndex(i).equalsIgnoreCase( DataSoal.getArrayIndex(i).getJawabanBenar() )){
                nilai += DataSoal.getArrayIndex(i).getNilai();
            }
        }
        getArrayIndex(indeks).setNilai(nilai);
        data.DataPendaftar.getArrayIndex(indeks).setTelahIkutTes(true);
    }
    
    public void hapusData() {
        int indeks = DataPendaftar.getIndeks_hapusData();
        getArrayJawaban().remove(indeks);
    }
    
    
}
