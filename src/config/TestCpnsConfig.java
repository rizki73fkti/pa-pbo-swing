package config;

public class TestCpnsConfig {
    private static boolean bukaTes = false;
    private static boolean bukaSeleksi = false;
    
    public static void setBukaTes(boolean bukaTes) {
        TestCpnsConfig.bukaTes = bukaTes;
    }

    public static void setBukaSeleksi(boolean bukaSeleksi) {
        TestCpnsConfig.bukaSeleksi = bukaSeleksi;
    }

    public static boolean isBukaTes() {
        return bukaTes;
    }
    
    public static boolean isBukaSeleksi() {
        return bukaSeleksi;
    }
    
    public static boolean isDapatBukaTes(){
        return !data.DataPendaftar.getArrayPendaftar().isEmpty() && !data.DataSoal.getArraySoal().isEmpty();
    }
}
